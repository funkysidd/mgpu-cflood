// textfile.cpp
//
// simple reading and writing for text files
//
// www.lighthouse3d.com
//
// You may use these functions freely.
// they are provided as is, and no warranties, either implicit,
// or explicit are given
//////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <string>
using namespace std;

#include "TextFile.h"

char *textFileRead(char *fn) {

  FILE *fp;
  char *content = NULL;

  int count = 0;

  if (fn != NULL) {				// Check to see if the function name is null or not
    fp = fopen(fn,"rt");		//Open a text file

    if (fp != NULL) {			//Check to see if the file is empty or not
      fseek(fp,0,SEEK_END);	//Seek the end of the file
      count = ftell(fp);		//Get the position of the end of file
      rewind(fp);				//Reset the pointer to the beginning of the file

      if (count>0) {
	content = (char *)malloc(sizeof(char)*(count+1));	//As much as i can understand now, this line allocates 
	//memory blocks for no. of lines +1 of type char and linked to the pointer "content"

	count = fread(content,sizeof(char),count,fp);		//Read data from a stream, fp
	content[count]='\0';//Replacing spaces at the location "count" with \0 -- 'escape sequence'
	//Strings ending in a null character are said to be null-terminated
      }
      fclose(fp);
    }
  }
  return content;
}

int textFileWrite(char *fn, char *s) {

  FILE *fp;
  int status = 0;

  if (fn != NULL) {
    fp = fopen(fn,"w");
    if (fp != NULL) {

      if (fwrite(s,sizeof(char),strlen(s),fp) == strlen(s))
	status = 1;
      fclose(fp);
    }
  }
  return(status);
}

void WriteOutputFile(float *dataarray, int nRows, int nCols, char *filename){
  string str("");
  char test[128];

  for(int i = 0; i < nRows; i++){
    for(int j = 0; j < nCols; j++){
      sprintf(test, "%f", dataarray[i * nCols + j]);
      str.append(test);
      str.append(" ");
    }
    str.append("\n");
  }

  textFileWrite(filename, (char *)str.c_str());
}
