
#include "DebugLog.h"

DebugLog::DebugLog(MPI_Comm comm, const char *name) {
  strcpy(log_name, name);
  memset(log_buf, 0, MAX_LOG_BUF_LENGTH);

  MPI_File_open(comm, log_name,
      MPI_MODE_WRONLY|MPI_MODE_CREATE, MPI_INFO_NULL,
      &fh);
}

DebugLog::~DebugLog() {
}

DebugLog DebugLog::operator<< (const char *data) {
  strcpy(log_buf, data);
  int buf_len = strlen(log_buf);

#if 1  
  MPI_File_write_ordered(fh, log_buf, buf_len, MPI_CHAR, &status);
#else
  MPI_File_write(fh, log_buf, buf_len, MPI_CHAR, &status);
#endif
}

void DebugLog::close() {
  MPI_File_close(&fh);
}

