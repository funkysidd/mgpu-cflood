
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;

#include "DataContainer.h"

using namespace std;

DataContainer::DataContainer() : data(NULL), kdTreePtr(NULL), dd_ptr(NULL),
  dim_x(0), dim_y(0), num_procs(0) {}

DataContainer::~DataContainer() {
  // Delete the KdTree pointer
  if (kdTreePtr) {  
    delete kdTreePtr;
    kdTreePtr = NULL;
  }

  // Delete the DataDecomposer pointer
  if (dd_ptr) {
    delete dd_ptr;
    dd_ptr = NULL;
  }

  // Delete 'data' container
  if (data) {
    for (int i=0; i<dim_y; i++) { 
      delete [] data[i];
      data[i] = NULL;
    }
  }

  delete [] data;
  data = NULL;
 
#if 0 
  // Delete the processor map
  for (int i=0; i<num_blks_y; i++) 
    delete [] proc_map[i];

  delete [] proc_map;
#endif
}

void DataContainer::instantiate(int dim_x, int dim_y, int num_procs) {
  // Just for disambiguation
  // this->blk_size_x = blk_size_x;
  // this->blk_size_y = blk_size_y;
  
  this->dim_x = dim_x;
  this->dim_y = dim_y;

  this->num_procs = num_procs;

  partitionAndGenerateOffsets();
}

void DataContainer::readDataFromFile(const char *dem_file) {
  // Allocate the data array (Is there a better way to allocate the array?) 
  data = new CONTAINER_TYPE* [dim_y];
  for (int i=0; i<dim_y; i++) 
    data[i] = new CONTAINER_TYPE [dim_x];

#if 1
  // Parse the file and copy the data
  int row = 0, col = 0;
  string line, tok;
  ifstream in_file(dem_file);

  if (in_file.is_open()) {
    while (in_file.good()) {
      getline(in_file, line);

      // Skip the header information
      if (line.find("ncols")!=string::npos)
	continue;
      else if(line.find("nrows")!=string::npos)
	continue;
      else if (line.find("cellsize")!=string::npos)
	continue;
      else if (line.find("xllcorner")!=string::npos)
	continue;
      else if (line.find("yllcorner")!=string::npos)
	continue;
      else if (line.find("NODATA_value")!=string::npos) 
	continue;
      else { // Parse the line for all columns in the row
        istringstream iss(line);
	col = 0;

	while (iss>>tok) {
	  // This test ensures we don't read more than what was allocated
	  if (row<dim_y&&col<dim_x)
#if 1 
	    data[row][col++] = atof(tok.c_str())*0.3048f; // feet to meter
#else
	    data[row][col++] = atof(tok.c_str()); // feet to meter
#endif
	}

	row++;
      }
    }

    in_file.close();
  }
#endif
}

void DataContainer::partitionAndGenerateOffsets() {
#if 0
  kdTreePtr = new KdTree(INT_PAIR(dim_x, dim_y), num_procs);
  proc_map = kdTreePtr->getProcAllocTable();
#else
  dd_ptr = new DataDecomposer(INT_PAIR(dim_x, dim_y), 1, num_procs);
  proc_map = (*dd_ptr).GetProcAllocTable();
#endif
      
#if 0
  printf("proc_alloc_table.size(): %ld\n", proc_alloc_table.size());  

  for (int i=0; i<proc_alloc_table.size(); i++) {
    for (int j=0; j<proc_alloc_table[i].size(); j++) {
      printf("%d \t", proc_alloc_table[i][j]);  
    }
    printf("\n");
  }
#endif

#if 0
  num_blks_x = kdTreePtr->getNumBlocksX(); 
  num_blks_y = kdTreePtr->getNumBlocksY(); 
#else
  num_blks_x = (*dd_ptr).GetNumBlocksX(); 
  num_blks_y = (*dd_ptr).GetNumBlocksY(); 
#endif
  
  // printf("num_blks_y: %d\n", num_blks_y); 

  // Initialize the offset map
  offset_map.resize(num_blks_y); 
  for (int i=0; i<num_blks_y; i++) {
    offset_map[i].resize(num_blks_x);
  }

  // Populate the offset map
#if 0
  int proc_count = 0, offset_x = 0, offset_y = 0;
  for (int i=0; i<num_blks_y; i++, offset_x=0) {
    for (int j=0; j<num_blks_x; j++) {
      if (proc_count>=num_procs)
	proc_count = 0;

      proc_map[i][j] = proc_count;
      proc_count++;

      offset_map[i][j] = INT_PAIR(offset_x, offset_y);
      offset_x += blk_size_x;
    }

    offset_y += blk_size_y;
  }
#else
  for (int i=0; i<num_blks_y; i++) {
    for (int j=0; j<num_blks_x; j++) {
#if 0
      const KdTreeNode *node = kdTreePtr->getProcNode(proc_map[i][j]);
#else
      const DecomposerNode *node = (*dd_ptr).GetProcNode(proc_map[i][j]);
#endif
      if (node)
#if 0
        offset_map[i][j] = node->min;
#else
        offset_map[i][j] = (*node).GetMin();
#endif
    }
  }
#endif
}

const vector< vector<int> >& DataContainer::getProcMap() const {
  return proc_map;
}
   
const vector< vector<INT_PAIR>  >& DataContainer::\
    getOffsetMap() const {
  return offset_map;
}

int DataContainer::getNumBlocksX() const {
  return num_blks_x;
}

int DataContainer::getNumBlocksY() const {
  return num_blks_y;
}

int DataContainer::getBlockSizeX(int i, int j) const {
  int proc = proc_map[i][j];
#if 0
  const KdTreeNode *node = kdTreePtr->getProcNode(proc);
#else
  const DecomposerNode *node = (*dd_ptr).GetProcNode(proc);
#endif
  if (node) {
#if 0
    INT_PAIR max = node->max;
    INT_PAIR min = node->min;
#else
    INT_PAIR min = (*node).GetMin();
    INT_PAIR max = (*node).GetMax();
#endif
    return (max[0]-min[0]);
  }
  else 
    return -1;
}

int DataContainer::getBlockSizeY(int i, int j) const {
  int proc = proc_map[i][j];
#if 0
  const KdTreeNode *node = kdTreePtr->getProcNode(proc);
#else
  const DecomposerNode *node = (*dd_ptr).GetProcNode(proc);
#endif
  if (node) {
#if 0
    INT_PAIR max = node->max;
    INT_PAIR min = node->min;
#else
    INT_PAIR min = (*node).GetMin();
    INT_PAIR max = (*node).GetMax();
#endif
    return (max[1]-min[1]);
  }
  else 
    return -1;
}

CONTAINER_TYPE const** DataContainer::getData() const {
  return (CONTAINER_TYPE const**)(data);
}
