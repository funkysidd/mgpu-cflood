
#include <stdlib.h>
#include <math.h>

#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;

#include "ComputeKernel.h"
#include "DataContainer.h"
#include "DataPacket.h"
#include "DebugLog.h"
#include "TextFile.h"

#define MAX_BLKS_PER_PROC 16

static void safe_exit(MPI_Comm comm) {
  MPI_Abort(comm, -1);
  MPI_Finalize();
}

static int FindSourceCol(float srcX, float xLowerLeftCorner, \
    float cellSize) {
  int srcCol;
  srcCol = ceil(((srcX-xLowerLeftCorner)/cellSize))-1; 
  return srcCol;
}

static int FindSourceRow(float srcY, float yLowerLeftCorner, \
    float cellSize, int nRows) {
  int srcRow;
  srcRow = ceil(((float)nRows-((srcY-yLowerLeftCorner)/cellSize)))-1;
  return srcRow;
}

static float FindSourceSlope(float east, float west, 
    float north, float south, \
    float cellSize) {
  float slopeEW;
  slopeEW = fabs(east-west)/cellSize;

  float slopeNS;
  slopeNS = fabs(north-south)/cellSize;

  return min(slopeEW, slopeNS);
}

static void ParseDEMHeader(char *dem_file, dem_header &dem_header_obj) {
  string line, tok;
  int bit_test = 1;
  ifstream in_file(dem_file);

  if (in_file.is_open()) {
    while (in_file.good()&&bit_test<64) {
      getline(in_file, line);
      istringstream iss(line);

      while (iss>>tok) {
	if (tok.compare("ncols")==0) {
	  iss>>tok;
          dem_header_obj.dim[0] = atoi(tok.c_str());
	  bit_test<<=1;
	}
	else if (tok.compare("nrows")==0) {
	  iss>>tok;
          dem_header_obj.dim[1] = atoi(tok.c_str());
	  bit_test<<=1;
	}
	else if (tok.compare("xllcorner")==0) {
	  iss>>tok;
          dem_header_obj.ll[0] = atof(tok.c_str());
	  bit_test<<=1;
	}
	else if (tok.compare("yllcorner")==0) {
	  iss>>tok;
          dem_header_obj.ll[1] = atof(tok.c_str());
	  bit_test<<=1;
	}
	else if (tok.compare("cellsize")==0) {
	  iss>>tok;
          dem_header_obj.cell_size = atof(tok.c_str());
	  bit_test<<=1;
	}
	else if (tok.compare("NODATA_value")==0) {
	  iss>>tok;
          dem_header_obj.no_data = atoi(tok.c_str());
	  bit_test<<=1;
	}
      }
    }

    in_file.close();
  }
}

int main(int argc, char *argv[]) {
  int num_procs, rank, name_len, buf_len, root=0;
  char process_name[MPI_MAX_PROCESSOR_NAME];
  char log_name[32], log_buf[4096];
  bool log_op = false, file_op = false;
  int flow_rows = 1, flow_cols = 2, num_iter = 1;
  int dim_x, dim_y, no_data;
  float mul_u = 1.f, mul_v = 1.f;
  float src_x_loc = 0.f, src_y_loc = 0.f;
  float x_ll = 0.f, y_ll = 0.f, cell_size = 1.f;
  char *dem_file = NULL, *hyg_file = NULL;

  MPI_File fh;
  MPI_Status status;
  MPI_Comm comm = MPI_COMM_WORLD;
  
  // Parse command line parameters
  for (int i=0; i<argc; i++) {
    if (strcmp(argv[i], "-log")==0) {
      log_op = true;
    }
    else if (strcmp(argv[i], "-fileop")==0) {
      file_op = true;
    }
    else if (strcmp(argv[i], "-east")==0) {
      mul_u = 1.f;
      mul_v = 0.f;  
    }
    else if (strcmp(argv[i], "-west")==0) {
      mul_u = -1.f;
      mul_v =  0.f;  
    }
    else if (strcmp(argv[i], "-north")==0) {
      mul_u = 0.f;
      mul_v = 1.f;  
    }
    else if (strcmp(argv[i], "-south")==0) {
      mul_u =  0.f;
      mul_v = -1.f;  
    }
    else if (strcmp(argv[i], "-numiter")==0) {
      num_iter = atoi(argv[++i]);
    }
    else if (strcmp(argv[i], "-dem")==0) {
      dem_file = argv[++i];
    }
    else if (strcmp(argv[i], "-hyg")==0) {
      hyg_file = argv[++i];
    }
    else if (strcmp(argv[i], "-flowrows")==0) {
      flow_rows = atoi(argv[++i]);
    }
    else if (strcmp(argv[i], "-dimx")==0) {
      dim_x = atoi(argv[++i]);
    }
    else if (strcmp(argv[i], "-dimy")==0) {
      dim_y = atoi(argv[++i]);
    }
    else if (strcmp(argv[i], "-srcx")==0) {
      src_x_loc = atof(argv[++i]);
    }
    else if (strcmp(argv[i], "-srcy")==0) {
      src_y_loc = atof(argv[++i]);
    }
  }

  // Initializations  
  MPI_Init(&argc, &argv);
  if (!DataTypes::init()) {
    safe_exit(comm);
  }
  
  // comm = MPI_COMM_WORLD;
  
  MPI_Comm_size(comm, &num_procs);
  MPI_Comm_rank(comm, &rank);
  
  // MPI_Get_processor_name(process_name, &name_len);
  
  sprintf(log_name, "debug.log");
  DebugLog debug_log(comm, log_name);
  
  char tmp_buf[1024];
  memset(tmp_buf, 0, 1024);
  
  // Defined for the entire system
  vector < vector <blk_meta_data> > send_meta_data_vec;
  vector <blk_meta_data> recv_meta_data_vec(MAX_BLKS_PER_PROC);
  vector <int> send_meta_data_cnt;
  blk_meta_data meta_data_obj;
  dem_header dem_header_obj;
  int recv_meta_data_cnt = 0, recv_blk_data_size = 0;
  int blk_size_x = 0, blk_size_y = 0;
  int lo[2], hi[2], buffer_size[2];
  int num_blks_x, num_blks_y; 
  char *dem_ptr = NULL, *hyg_ptr = NULL, *tok = NULL;
  
  if (rank==root) {
    // Read the header info from the DEM file
#if 1
    ParseDEMHeader(dem_file, dem_header_obj);
#else
    dem_ptr = textFileRead(dem_file);
    tok = strtok(dem_ptr, " ");
    tok = strtok(NULL, " ");
    dem_header_obj.dim[0] = atoi(tok);
    tok = strtok(NULL, " ");
    dem_header_obj.dim[1] = atoi(tok);
    tok = strtok(NULL, " ");
    dem_header_obj.ll[0] = atof(tok);
    tok = strtok(NULL, " ");
    dem_header_obj.ll[1] = atof(tok);
    tok = strtok(NULL, " ");
    dem_header_obj.cell_size = atof(tok);
    tok = strtok(NULL, " ");
    dem_header_obj.no_data = atoi(tok);
#endif
  }
    
  MPI_Barrier(comm); // Not sure if needed

  // printf("dem_header_obj.ll[0]: %0.4f\n", dem_header_obj.ll[0]);

  // Broadcast the header information
  MPI_Bcast((void *)(&dem_header_obj), 1, DataTypes::MPI_DEM_HEADER, root, comm);

  x_ll = dem_header_obj.ll[0];
  y_ll = dem_header_obj.ll[1];
  cell_size = dem_header_obj.cell_size;
  no_data = dem_header_obj.no_data; 
    
  sprintf(tmp_buf, "x_ll: %0.4f\n", x_ll);
  debug_log << tmp_buf;

  sprintf(tmp_buf, "y_ll: %0.4f\n", y_ll);
  debug_log << tmp_buf;

  sprintf(tmp_buf, "cell_size: %0.4f\n", cell_size);
  debug_log << tmp_buf;

  // printf("Test-1\n");
  
  // Create an empty data container
  DataContainer dataContainer;

  // Instantiate the container and get the offset map
  dataContainer.instantiate(dim_x, dim_y, num_procs);
  vector < vector <INT_PAIR>  > const &offset_map =
    dataContainer.getOffsetMap();
  
  vector< vector<int> > proc_map = dataContainer.getProcMap();

  num_blks_x = dataContainer.getNumBlocksX();
  num_blks_y = dataContainer.getNumBlocksY();
  
  // printf("Test-2\n");

  if (rank==root) {
    // Resize to the number of processes
    send_meta_data_vec.resize(num_procs); 
    send_meta_data_cnt.resize(num_procs, 0); 
    
    for (int i=0; i<num_blks_y; i++) {
      for (int j=0; j<num_blks_x; j++) {
	// The coresponding process assigned to this block
	int dest = proc_map[i][j];	
	send_meta_data_cnt[dest] += 1; // Increment the count by 1
      }
    }

    for (int i=0; i<num_blks_y; i++) {
      for (int j=0; j<num_blks_x; j++) {
	// The corresponding process and offset in the dataset
	int dest = proc_map[i][j];	
	INT_PAIR offset = offset_map[i][j];

	blk_size_x = dataContainer.getBlockSizeX(i, j);
	blk_size_y = dataContainer.getBlockSizeY(i, j);

	meta_data_obj.offset[0] = lo[0] = offset[0]; // j, x
	meta_data_obj.offset[1] = lo[1] = offset[1]; // i, y

	hi[0] = ((lo[0]+blk_size_x)>dim_x)?dim_x:lo[0]+blk_size_x; 
	hi[1] = ((lo[1]+blk_size_y)>dim_y)?dim_y:lo[1]+blk_size_y;

	meta_data_obj.extent[0] = buffer_size[0] = hi[0]-lo[0];
	meta_data_obj.extent[1] = buffer_size[1] = hi[1]-lo[1];

	meta_data_obj.id[0] = j;
        meta_data_obj.id[1] = i;	

        send_meta_data_vec[dest].push_back(meta_data_obj); 
      }
    }
 
    for (int i=0; i<num_procs; i++) {
      if (i!=root) {
	// printf("send_meta_data_cnt[%d]: %d\n", i, send_meta_data_cnt[i]);
	MPI_Send((void *)&(send_meta_data_vec[i][0]), send_meta_data_cnt[i],
	    DataTypes::MPI_BLK_META_DATA, i, 123,
	    comm);
      } 
      else {
	// Copy the data to the recv_buf
	// memcpy(recv_buf, &(send_buf[0][0]), send_cnt[i]*sizeof(int));
	recv_meta_data_vec = send_meta_data_vec[i];
	recv_meta_data_cnt = send_meta_data_cnt[i];
      }	
    }
  } // End of (rank==root)
  else {
    MPI_Recv((void *)&(recv_meta_data_vec[0]), MAX_BLKS_PER_PROC,
	DataTypes::MPI_BLK_META_DATA, root, 123, comm,
	&status);

    MPI_Get_count(&status, DataTypes::MPI_BLK_META_DATA, &recv_meta_data_cnt);
    // printf("recv_meta_data_cnt: %d\n", recv_meta_data_cnt);
  }

  MPI_Barrier(comm);
  
  // printf("Test-3\n");
  // printf("recv_meta_data_cnt: %d\n", recv_meta_data_cnt);

  /**
  Use the same log file, as a shared resource, to write to by all processes.
  */

  // Number of blocks received by this process
  int blk_buf_size = 0;
  CONTAINER_TYPE **blk_data_buf = new CONTAINER_TYPE* [recv_meta_data_cnt]; 
  
  // char *buf_ptr = log_buf;
  
  /**
  TODO-1: Instead of creating a 2D array, create a large 1D array which can then
  be used by MPI_Recv. Also create a 2D array of pointers which point to
  locations in the large 1D array, and use that to access blocks assigned to a
  proc. This can be abstracted into a class too.

  TODO-2: Change variable names to better identify the block/ proc variables.
  */

  for (int i=0 ; i<recv_meta_data_cnt; i++) {
    meta_data_obj = recv_meta_data_vec[i];
    if (log_op) {
      sprintf(tmp_buf, "proc %.4d: (%d, %d)\n", rank,
	  meta_data_obj.extent[0], meta_data_obj.extent[1]);

      // buf_len = strlen(tmp_buf);
      // memcpy(buf_ptr, tmp_buf, buf_len);
      // buf_ptr += buf_len; 
      // memset(tmp_buf, 0, 1024);

      debug_log << tmp_buf;
    }

    blk_buf_size += meta_data_obj.extent[0]*\
		    meta_data_obj.extent[1];
  }
  
  // printf("Test-4\n");
  // printf("blk_buf_size: %d\n", blk_buf_size); 

  CONTAINER_TYPE *blk_data_ptr = new CONTAINER_TYPE [blk_buf_size];

  blk_buf_size = 0;
  for (int i=0; i<recv_meta_data_cnt; i++) {
    meta_data_obj = recv_meta_data_vec[i];

    blk_data_buf[i] = blk_data_ptr+blk_buf_size;
    blk_buf_size += meta_data_obj.extent[0]*\
		    meta_data_obj.extent[1];
  }
  
  // printf("Test-5\n");
    
  // Note: Now distribute the dataset among all the nodes, using the mapping
  // from the proc_map.
 
#if 1 
  if (rank==root) {
    // Allocate the data array and read in the data from the file
    printf("dem_file: %s\n", dem_file);
    dataContainer.readDataFromFile(dem_file);
    printf("dem file read\n");

    CONTAINER_TYPE const** data = dataContainer.getData();

#if 0
    for (int i=0; i<dim_y; i++) {
      // for (int j=0; j<dim_x; j++) {
	printf("%0.4f %0.4f", data[i][0], data[i][dim_x-1]); 
      // }
      printf("\n");
    }
#endif

    // Create a 1D data block from the 2D data representation
    CONTAINER_TYPE **proc_data = new CONTAINER_TYPE* [num_procs];
    vector <int> proc_buf_size(num_procs);
    vector <int> displs(num_procs);

    for (int i=0; i<num_procs; i++) {
      int tmp_buf_size = 0;
      for (int j=0; j<send_meta_data_vec[i].size(); j++) {
        meta_data_obj = send_meta_data_vec[i][j];
	buffer_size[0] = meta_data_obj.extent[0];
	buffer_size[1] = meta_data_obj.extent[1];

	tmp_buf_size += buffer_size[0]*buffer_size[1];
      }

      proc_data[i] = new CONTAINER_TYPE[tmp_buf_size];
      proc_buf_size[i] = tmp_buf_size;
      displs[i] = 0;
    }

    // Copy from 'data' -> 'proc_data'
    for (int i=0; i<num_blks_y; i++) {
      for (int j=0; j<num_blks_x; j++) {
	int dest = proc_map[i][j];	
	INT_PAIR offset = offset_map[i][j];

	lo[0] = offset[0]; // j, x
	lo[1] = offset[1]; // i, y
	
	blk_size_x = dataContainer.getBlockSizeX(i, j);
	blk_size_y = dataContainer.getBlockSizeY(i, j);

	hi[0] = ((lo[0]+blk_size_x)>dim_x)?dim_x:lo[0]+blk_size_x; 
	hi[1] = ((lo[1]+blk_size_y)>dim_y)?dim_y:lo[1]+blk_size_y;
	
	buffer_size[0] = hi[0]-lo[0];
	buffer_size[1] = hi[1]-lo[1];
	
	for (int k=lo[1]; k<hi[1]; k++) {
          CONTAINER_TYPE *dest_buf = proc_data[dest]+displs[dest];
	  memcpy(dest_buf, &(data[k][lo[0]]),
	      buffer_size[0]*sizeof(CONTAINER_TYPE));
	  displs[dest] += buffer_size[0];
	}
      }
    }

    // Send the data 
    for (int i=0; i<num_procs; i++) {
      if (i!=root) { 
        MPI_Send(proc_data[i], proc_buf_size[i], MPI_FLOAT, i, 123, comm);
      }
      else { // Copy the data into the recv buffer
	memcpy(blk_data_ptr, proc_data[root],
	    proc_buf_size[root]*sizeof(CONTAINER_TYPE));

	recv_blk_data_size = proc_buf_size[root];
      }

      delete [] proc_data[i];
    }

    delete [] proc_data;
  }
  else {
    MPI_Recv(blk_data_ptr, blk_buf_size, MPI_FLOAT, root, 123, comm, &status);
    
    MPI_Get_count(&status, MPI_FLOAT, &recv_blk_data_size);
  }
  
  MPI_Barrier(comm);
    
  if (recv_blk_data_size!=blk_buf_size) {
    sprintf(tmp_buf, "Rank %d: error in receiving data\n", rank);
    debug_log << tmp_buf;
  }
  else {
    sprintf(tmp_buf, "Rank %d: success in receiving data\n", rank);
    debug_log << tmp_buf; 
  }
#endif
  
  // printf("Test-6\n");
 
  // Create DataPacket associated with this process
  meta_data_obj = recv_meta_data_vec[0];
 
  // Packet specific meta data
  int p_offset[2], p_dim[2], p_id[2];

  p_offset[0] = meta_data_obj.offset[0];
  p_offset[1] = meta_data_obj.offset[1];

  p_dim[0] = meta_data_obj.extent[0]; // width
  p_dim[1] = meta_data_obj.extent[1]; // height

  p_id[0] = meta_data_obj.id[0]; 
  p_id[1] = meta_data_obj.id[1];

  // printf("p_id[0]: %d\n", p_id[0]); 
  // printf("p_id[1]: %d\n", p_id[1]); 

  // Neighboring process map; the current process goes to the middle of the
  // 2-D array.
  vector< vector<int> >  n_procs(3, vector<int>(3, -1));
  for (int i=0; i<3; i++) {
    for (int j=0; j<3; j++) {
      int _i = p_id[1]+i-1;
      int _j = p_id[0]+j-1;

      if (_i>=0&&_i<proc_map.size()) { 
	if (_j>=0&&_j<proc_map[0].size()) {
          n_procs[i][j] = proc_map[_i][_j];
	}
      }
    }
  }

#if 1
  if (log_op) {
    char char_arr[16];
    strcpy(tmp_buf, "");
    for (int i=0; i<3; i++) {
      for (int j=0; j<3; j++) {
	sprintf(char_arr, "%d ", n_procs[i][j]);
	strcat(tmp_buf, char_arr);
      }

      strcat(tmp_buf, "\n");
    }

    debug_log << tmp_buf; 
  }
#endif

  INT_PAIR ghost_cells(1, 1);
  DataPacket dem_values(p_dim[0], p_dim[1], ghost_cells);
  if (!dem_values.init(blk_data_buf[0])) {
    printf("DataPacket initialization failed!!!\n");
    safe_exit(comm);
  }

  // TODO: All these variables should be placed into a global structure
  float h_extra = 0.001f;
  float sim_time = 0.f, dt = 0.1f;
  float man_n = 0.013f;
  float g = 9.81f;
  float dx = cell_size, dy = cell_size;
  float courant_no = 0.1f;
  float src_discharge = 0.f;
  float src_slope = 0.f;
  float h_src = 0.f, u_src = 0.f, v_src = 0.f;
  float max_u_vel = 0.f, max_v_vel = 0.f, hx_cn = 0.f, hy_cn = 0.f;
  int hydro_counter = 0, hydro_lo = 0, hydro_hi = 1;

  // The process which updates the source values
  int src_update_proc = -1;

  // These row and col values are wrt to the whole dataset
  int src_col = FindSourceCol(src_x_loc, x_ll, cell_size);
  int src_row = FindSourceRow(src_y_loc, y_ll, cell_size, dim_y);
  
  if ((src_row>=p_offset[1]&&src_row<p_offset[1]+p_dim[1])&&
      (src_col>=p_offset[0]&&src_col<p_offset[0]+p_dim[0])) {
    printf("Calculating source slope on process: %d\n", rank);
    src_update_proc = rank;
  }
  
  // Note: The hydrograph should ideally be needed on 'src_update_proc' only.
  // However, we are going to broadcast it to all the processes.

  float *hyg_values = new float[flow_rows*flow_cols];
#if 1
  if (rank==root) {
    printf("hyg_file: %s\n", hyg_file);
    printf("flow_rows: %d\n", flow_rows);
    hyg_ptr = textFileRead(hyg_file);
    printf("hyg file read\n");
    tok = strtok(hyg_ptr, "\n");
    tok = strtok(NULL, "\n");
    for (int i=0; i<flow_rows; i++) {
      tok = strtok(NULL, ",");
      hyg_values[i*flow_cols+0] = atof(tok)*0.028316847; // cfs to cms
      tok = strtok(NULL, "\n");
      hyg_values[i*flow_cols+1] = atof(tok);
    }
  }
#endif

 
#if 1
  MPI_Barrier(comm); // Not sure if needed

  if (MPI_Bcast((void *)hyg_values, flow_rows*flow_cols, MPI_FLOAT, root,
	comm)!=MPI_SUCCESS) {
    printf("MPI_Bcast failed\n");
    safe_exit(comm);
  }
#endif
  
  /*if (rank==src_update_proc) {
    for (int i=0; i<flow_rows;i++) {
      printf("%0.4f %0.4f\n", hyg_values[i*flow_cols+0],
	  hyg_values[i*flow_cols+1]);
    }
  }*/
  
  float max_hydro_time = hyg_values[((flow_rows-1)*flow_cols)+1]*3600.f;
  
  // printf("max hydro time: %0.4f\n", max_hydro_time);

  if (rank==src_update_proc)  {
    printf("src_row: %d, src_col: %d\n", src_row, src_col);

    // Adjust for row and col values relative to this block
    src_col -= p_offset[0];
    src_row -= p_offset[1];

    // Account for the ghost cells
    src_col += ghost_cells[0];
    src_row += ghost_cells[1];

    float src_e = dem_values.access_data(src_row, src_col+1);
    float src_w = dem_values.access_data(src_row, src_col-1);
    float src_n = dem_values.access_data(src_row-1, src_col);
    float src_s = dem_values.access_data(src_row+1, src_col);

    src_slope = FindSourceSlope(src_e, src_w, src_n, src_s,
	cell_size);
    
    printf("src_row: %d, src_col: %d\n", src_row, src_col);
    printf("src_e: %0.4f, src_w: %0.4f\n", src_e, src_w);
    printf("src_n: %0.4f, src_s: %0.4f\n", src_n, src_s);

    if (src_slope==0.f) {
      src_slope = h_extra;
      printf("Source slope is 0, set to %0.4f\n", h_extra);
    }

    float tmp_h_src_term =
      (hyg_values[0*flow_cols+0]*man_n)/(cell_size*sqrt(src_slope));

    float h_src_term = pow(tmp_h_src_term, 0.6f);

    if (h_src_term==0.f)
      v_src = src_discharge/(cell_size*(h_src_term+h_extra));
    else
      v_src = src_discharge/(cell_size*h_src_term);
    
    printf("Inital v_src: %0.4f\n", v_src);
  }
   
  MPI_Barrier(comm); // Not sure if needed

  DataPacket h_old(p_dim[0], p_dim[1], ghost_cells);
  DataPacket h_new(p_dim[0], p_dim[1], ghost_cells);
  
  DataPacket u_old(p_dim[0], p_dim[1], ghost_cells);
  DataPacket u_new(p_dim[0], p_dim[1], ghost_cells);

  DataPacket v_old(p_dim[0], p_dim[1], ghost_cells);
  DataPacket v_new(p_dim[0], p_dim[1], ghost_cells);

  h_old.init(h_extra);
  h_new.init(0.f);
  
  u_old.init(0.f);
  u_new.init(0.f);

  v_old.init(0.f);
  v_new.init(0.f);

  // The neighborhood info for this process
  dem_values.setProcMap(n_procs);

  h_old.setProcMap(n_procs);
  h_new.setProcMap(n_procs);
  
  u_old.setProcMap(n_procs);
  u_new.setProcMap(n_procs);
  
  v_old.setProcMap(n_procs);
  v_new.setProcMap(n_procs);

  // Array of pointers to ping-point between data packets
  DataPacket *h_buffer[2] = {&h_old, &h_new};
  DataPacket *u_buffer[2] = {&u_old, &u_new};
  DataPacket *v_buffer[2] = {&v_old, &v_new};

  // Note: This step ensures ghost cell values are from neighboring cells and
  // not copied from internal cells.

  dem_values.interchange(comm, rank);

  /*if (rank==src_update_proc) {
    int test_w = dem_values.GetPaddedWidth();
    int test_h = dem_values.GetPaddedHeight();

    for (int i=0; i<test_h; i++) {
      for (int j=0; j<test_w; j++) {
	printf("%0.4f ", dem_values.access_data(i, j));
      }
      printf("\n");
    }
  }*/
  
  sprintf(tmp_buf, "Rank: %d, [%d][%d], [%d][%d], [%d][%d], [%d][%d]\n", \
    rank, 0, 0, p_dim[1]+1, 0, 0, p_dim[0]+1, p_dim[1]+1, p_dim[0]+1);

  debug_log << tmp_buf;

  sprintf(tmp_buf, "Rank: %d, [0][0]: %0.4f, [0][1]: %0.4f, [1][0]: %0.4f, [1][1]: %0.4f\n", \
    rank, dem_values.access_data(0, 0), dem_values.access_data(p_dim[1]+1, 0), \
    dem_values.access_data(0,p_dim[0]+1), dem_values.access_data(p_dim[1]+1, \
    p_dim[0]+1));

  debug_log << tmp_buf;
 
  // Update h, u, and v values on the source cell process
  if (rank==src_update_proc) {
    h_buffer[0]->access_data(src_row, src_col) = h_src;
    u_buffer[0]->access_data(src_row, src_col) = mul_u*v_src;
    v_buffer[0]->access_data(src_row, src_col) = mul_v*v_src;
  }
 
#if 0
  // Interchange test
  DataPacket test_packet(5, 5, ghost_cells);
  test_packet.init(0.f);
  test_packet.incr(rank);
  test_packet.setProcMap(n_procs);
  test_packet.interchange(comm, rank);
  
  if (log_op) {
    char char_arr[16];
    sprintf(tmp_buf, "Rank: %d\n", rank);
    for (int i=0; i<7; i++) {
      for (int j=0; j<7; j++) {
        float tmp_val = test_packet.access_data(i, j);
	sprintf(char_arr, "%0.1f ", tmp_val);
	strcat(tmp_buf, char_arr);
      }
      strcat(tmp_buf, "\n");
    }

    debug_log << tmp_buf;
  }
      
  /*if (!(test_packet.WriteToDisk(rank, "test_packet", 0))) {
    if (log_op) {
      sprintf(tmp_buf, "Rank: %d - write to disk failed on test_packet!!!\n",
	  rank);
      debug_log << tmp_buf;
    }
  }*/

  ScalarField testField;
  testField.InitSize(5, 5, 1);
  testField.InitHost();
#endif

  MPI_Barrier(comm); // Not sure if needed
 
#if 1 
  // Solve the equations for a given number of iterations
  for (int i=0, file_cnt=0; i<num_iter; i++) {
    DataPacket &h_i = *(h_buffer[(i+0)%2]);
    DataPacket &h_o = *(h_buffer[(i+1)%2]);
    
    DataPacket &u_i = *(u_buffer[(i+0)%2]);
    DataPacket &u_o = *(u_buffer[(i+1)%2]);
    
    DataPacket &v_i = *(v_buffer[(i+0)%2]);
    DataPacket &v_o = *(v_buffer[(i+1)%2]);

    // Reset the max and the 'h 'values to zero
    max_u_vel = max_v_vel = 0.f;
    hx_cn = hy_cn = 0.f;

    ComputeKernel::flood_equation_cpu(h_i, h_o, u_i, u_o, v_i, v_o, dem_values,
	dx, dy, dt, h_extra, man_n, g, rank, src_update_proc, src_row, src_col,
	max_u_vel, max_v_vel, hx_cn, hy_cn); 

    vel_height _max_vel_height[2];
    _max_vel_height[0].vel[0] = max_u_vel;
    _max_vel_height[0].vel[1] = max_v_vel;
    _max_vel_height[0].height[0] = hx_cn;
    _max_vel_height[0].height[1] = hy_cn;

    /*if (log_op) {
      sprintf(tmp_buf, "Before: u_vel: %0.4f v_vel: %0.4f, hx_cn: %0.4f, hy_cn: %0.4f\n",
	  max_u_vel, max_v_vel, hx_cn, hy_cn);
      debug_log << tmp_buf;
    }*/

    // In place reduce; all processes get the same max vel and height values
    MPI_Allreduce(&_max_vel_height[0], &_max_vel_height[1], 1, DataTypes::MPI_VEL_HEIGHT,
	DataTypes::MPI_VEL_HEIGHT_MAX, comm);

    max_u_vel = _max_vel_height[1].vel[0];
    max_v_vel = _max_vel_height[1].vel[1];
    hx_cn = _max_vel_height[1].height[0];
    hy_cn = _max_vel_height[1].height[1];
        
    float dt_x = (dx/(max_u_vel+sqrt(g*(hx_cn+h_extra))));
    float dt_y = (dy/(max_v_vel+sqrt(g*(hy_cn+h_extra))));

    float temp_cn = (dt_x<dt_y)?dt_x:dt_y;
    float dt_check = courant_no*temp_cn; 

    // Note: The dt values calculated on each of the processes "may be" slightly
    // different in precision. It may be appropriate to calculate the dt value
    // on 'src_update_proc', and use it on all other processes.

    dt = dt_check;
    sim_time += dt; 

    if (rank==0) {
      printf("dt: %0.4f, sim_time: %0.4f\n", dt, sim_time); 
    }

    /*if (log_op) {
      sprintf(tmp_buf, "After: u_vel: %0.4f v_vel: %0.4f, hx_cn: %0.4f, hy_cn:\
	  %0.4f, dt:%0.4f\n", max_u_vel, max_v_vel, hx_cn, hy_cn, dt);
      debug_log << tmp_buf;
    }*/
    
    // Calculate the new h_src, u_src, v_src values, update the source cell
    // process, and call interchange on h_o, u_o, v_o. We need to perform this
    // step on 'src_update_proc' only.

    if (rank==src_update_proc) {
      if (sim_time<max_hydro_time) {
	float tmp_val1 = hyg_values[hydro_counter*flow_cols+1];
	if (sim_time>/*ACCESS(hyg_values, hydro_counter, 1, flow_cols)*/tmp_val1*3600.f) {
	  for (int j=hydro_counter; j<flow_rows; j++) {
	    float tmp_val2 = hyg_values[j*flow_cols+1];
	    if (sim_time</*ACCESS(hyg_values, j, 1, flow_cols)*/tmp_val2*3600.f) {
	      hydro_hi = j;
	      hydro_lo = j-1;
	      break;
	    }
	  }

	  hydro_counter = hydro_lo;

	  float q_l = hyg_values[hydro_lo*flow_cols+0];
	  float q_h = hyg_values[hydro_hi*flow_cols+0];
	  float t_l = hyg_values[hydro_lo*flow_cols+1];
	  float t_h = hyg_values[hydro_hi*flow_cols+1];
	  float t_x = sim_time/3600.f;

	  src_discharge = q_l+(((q_h-q_l)/(t_h-t_l))*(t_x-t_l));
	  float discharge_term = src_discharge*man_n/(cell_size*sqrt(src_slope));
	  h_src = pow(discharge_term, 0.6f);

	  if (h_src==0.f) {
	    v_src = src_discharge /(cell_size*(h_src+h_extra));
	  }
	  else {
	    v_src = src_discharge /(cell_size*h_src);
	  }
	  
	  // printf("h: %0.4f, u: %0.4f, v: %0.4f lo: %d hi: %d\n", h_src, v_src,
	  //     v_src, hydro_lo, hydro_hi); 

	  // printf("h_i-0: %0.4f\n", h_i.access_data(src_row, src_col));

	  h_o.access_data(src_row, src_col) = h_src;
	  u_o.access_data(src_row, src_col) = mul_u*v_src;
	  v_o.access_data(src_row, src_col) = mul_v*v_src;
	  
	  // printf("h_src: %0.4f\n", h_src);
	  // printf("h_o-1: %0.4f\n", h_o.access_data(src_row, src_col));
	  // printf("u_o-1: %0.4f\n", u_o.access_data(src_row, src_col));
	  // printf("v_o-1: %0.4f\n", v_o.access_data(src_row, src_col));
	}
      }
      else {
	printf("WARNING: Simulation time exceeded maximum hydro time!!!\n"); 
	src_discharge = 0.f;
	h_src = 0.f;
	v_src = 0.f;
      }
    }

    MPI_Barrier(comm);
    
    if (!(h_o.interchange(comm, rank))) {
      if (log_op) {
	sprintf(tmp_buf, "Rank: %d iter: %d - data interchange failed on \
	    h_o!!!\n", rank, i);
	debug_log << tmp_buf;
      }
    }

    if (!(u_o.interchange(comm, rank))) {
      if (log_op) {
	sprintf(tmp_buf, "Rank: %d iter: %d - data interchange failed on \
	    u_o!!!\n", rank, i);
	debug_log << tmp_buf;
      }
    }

    if (!(v_o.interchange(comm, rank))) {
      if (log_op) {
	sprintf(tmp_buf, "Rank: %d iter: %d - data interchange failed on \
	    v_o!!!\n", rank, i);
	debug_log << tmp_buf;
      }
    }

    if ((i%500==0)&&file_op) {
      if (!(h_o.WriteToDisk(rank, "h_out", file_cnt))) {
	if (log_op) {
	  sprintf(tmp_buf, "Rank: %d iter: %d - write to disk failed on h_o!!!\n",
	      rank, i);
	  debug_log << tmp_buf;
	}
      }
	
      file_cnt++;
    }

    MPI_Barrier(comm);
  }
#endif

  // Release allocated memory - keep this around for now, we may need the buffer
  // to send the data back to the root process. 
  delete [] blk_data_ptr; 
  delete [] blk_data_buf;

  if (dem_ptr)
    free(dem_ptr);

  if (hyg_ptr)
    free(hyg_ptr);

  if (hyg_values)
    delete [] hyg_values;

  debug_log.close();
  
  MPI_Finalize();

  return 0;
}
