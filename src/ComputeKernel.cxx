
#include <stdlib.h>
#include <math.h>

#include "ComputeKernel.h"

void ComputeKernel::flood_equation_cpu(DataPacket &h_i, DataPacket &h_o, \
      DataPacket &u_i, DataPacket &u_o, \
      DataPacket &v_i, DataPacket &v_o, \
      DataPacket &dem_values, \
      float dx, float dy, float dt, \
      float hExtra, float manN, float g, \
      int rank, int src_update_proc, \
      int src_row, int src_col, \
      float &maxUvel, float &maxVvel, \
      float &hxCn, float &hyCn) {

  int new_w = h_i.GetPaddedWidth();
  int new_h = h_i.GetPaddedHeight();
  INT_PAIR ghost_cells = h_i.GetGhostCells();

  // printf("new_w: %d\n", new_w);
  // printf("new_h: %d\n", new_w);
  // printf("ghost_cells[0]: %d\n", ghost_cells[0]);
  // printf("ghost_cells[1]: %d\n", ghost_cells[1]);

  float denomX = 0.f, denomY = 0.f, exp = 0.f;
  float ubarij = 0.f, vbarij = 0.f, hbarX = 0.f, hbarY = 0.f;
  float hij = 0.f, hi_p_1j = 0.f, hi_m_1j = 0.f, hij_p_1 = 0.f, hij_m_1  = 0.f;
  float uij = 0.f, ui_m_1j = 0.f, ui_p_1j = 0.f, ui_m_1j_p_1 = 0.f, uij_p_1 =
    0.f, uij_m_1 = 0.f;
  float vij = 0.f, vi_m_1j = 0.f, vi_p_1j = 0.f, vi_p_1j_m_1 = 0.f, vij_m_1 =
    0.f, vij_p_1 = 0.f;
  float elevij = 0.f, elevi_p_1j = 0.f, elevij_p_1 = 0.f;
  float duDx = 0.f, duDy = 0.f, dvDx = 0.f, dvDy = 0.f, dHdx = 0.f, dHdy = 0.f,
	sFx = 0.f, sFy = 0.f;
  float duhDx = 0.f, dvhDy = 0.f;
  float rhsU = 0.f, rhsV = 0.f;
  float uNew = 0.f, vNew = 0.f, hNew = 0.f;
  float hE = 0.f, hW = 0.f, hN = 0.f, hS = 0.f;

  for (int row=ghost_cells[1]; row<new_h-ghost_cells[1]; row++) {
    for (int col=ghost_cells[0]; col<new_w-ghost_cells[0]; col++) {

      // Skip the src_row, src_col cell
      if (rank==src_update_proc) {
	if (row==src_row&&col==src_col) {
	  // printf("row: %d col: %d skipped on proc: %d\n", row, col, rank);
	  continue;
	}
      }

      hij         = h_i.access_data(row,     col    ); 
      hi_p_1j     = h_i.access_data(row,     col + 1);
      hi_m_1j     = h_i.access_data(row,     col - 1);
      hij_p_1     = h_i.access_data(row - 1, col    );
      hij_m_1     = h_i.access_data(row + 1, col    );

      uij         = u_i.access_data(row,     col    );
      ui_m_1j     = u_i.access_data(row,     col - 1);
      ui_p_1j     = u_i.access_data(row,     col + 1);
      ui_m_1j_p_1 = u_i.access_data(row - 1, col - 1);
      uij_p_1     = u_i.access_data(row - 1, col    );
      uij_m_1     = u_i.access_data(row + 1, col    );

      vij         = v_i.access_data(row,     col    );
      vi_m_1j     = v_i.access_data(row,     col - 1);
      vi_p_1j     = v_i.access_data(row,     col + 1);
      vi_p_1j_m_1 = v_i.access_data(row + 1, col + 1);
      vij_m_1     = v_i.access_data(row + 1, col    );
      vij_p_1     = v_i.access_data(row - 1, col    );

      elevij     = dem_values.access_data(row,     col    );
      elevi_p_1j = dem_values.access_data(row,     col + 1);
      elevij_p_1 = dem_values.access_data(row - 1, col    );

      ubarij = (uij+ui_m_1j+ui_m_1j_p_1+uij_p_1)/4.f;
      vbarij = (vij+vi_p_1j+vi_p_1j_m_1+vij_m_1)/4.f;

      // Upwinding u derivative, hE, hW
      if (uij>0.f) {
	duDx = (uij-ui_m_1j)/dx ; // Momentum upwinding condition
	hE = hij; // Continuity equation upwinding condition in x-direction
      }
      else {
	duDx = (ui_p_1j-uij)/dx;
	hE = hi_p_1j; // Continuity equation upwinding condition in x-direction
      }

      if (vbarij>0.f) {
	duDy = (uij-uij_m_1)/dy;
      }
      else {
	duDy = (uij_p_1-uij)/dy;
      }

      if (ui_m_1j>0.f) {
	hW = hi_m_1j; // Continuity equation upwinding condition in x-direction
      }
      else {
	hW = hij; // Continuity equation upwinding condition in x-direction
      }

      // Upwinding v derivative, hN, hS
      if (ubarij>0.f) {
	dvDx = (vij-vi_m_1j)/dx;
      }
      else {
	dvDx = (vi_p_1j-vij)/dx;
      }

      if (vij>0.f) {
	dvDy = (vij-vij_m_1)/dy; // Momentum upwinding condition
	hN = hij; // Continuity equation upwinding condition in y-direction
      }
      else {
	dvDy = (vij_p_1-vij)/dy;
	hN = hij_p_1; // Continuity equation upwinding condition in y-direction
      }

      if (vij_m_1>0.f) {
	hS = hij_m_1; // Continuity equation upwinding condition in y-direction
      }
      else {
	hS = hij; // Continuity equation upwinding condition in y-direction
      }

      dHdx = ((elevi_p_1j+hi_p_1j)-(elevij+hij))/dx;
      dHdy = ((elevij_p_1+hij_p_1)-(elevij+hij))/dy;

      // Calculate friction slopes sFx and sFy using Manning's n.
      // Constant roughness used here
      hbarX = (hij+hi_p_1j)/2.f;
      hbarY = (hij+hij_p_1)/2.f;

      exp = 4.f/3.f;
      denomX = pow((hbarX+hExtra), exp);
      sFx = (manN*manN)*uij*sqrt(uij*uij+vbarij*vbarij)/denomX;
      denomY = pow((hbarY+hExtra), exp);
      sFy = (manN*manN)*vij*sqrt(vij*vij+ubarij*ubarij)/denomY;

      // Momentum update
      rhsU = dt*(uij*duDx+vbarij*duDy+g*dHdx+g*sFx);
      rhsV = dt*(ubarij*dvDx+vij*dvDy+g*dHdy+g*sFy);

      uNew = uij-rhsU;
      vNew = vij-rhsV;

      // Check for higher velocities (due to numerical oscillations etc.)
      if ((uNew>100.f)||(uNew<-100.f)) {
	uNew = 0.f;
      } 

      if ((vNew>100.f)||(vNew<-100.f)) {
	vNew = 0.f;
      }

      // Continuity update
      duhDx = (uij*hE-ui_m_1j*hW)/dx; // RHS side of the Continuity
      dvhDy = (vij*hN-vij_m_1*hS)/dy; // RHS side of the Continuity
      hNew = hij-dt*(duhDx+dvhDy); // Continuity explicit time update

      if ((hNew<0.001)&&((fabs(uNew)>1.f)||(fabs(vNew)>1.f))) {
	hNew = 0.f;
	uNew = 0.f;
	vNew = 0.f;
      }

      // Set the new values
      h_o.access_data(row, col) = hNew;
      u_o.access_data(row, col) = uNew;
      v_o.access_data(row, col) = vNew;
           
      // The maximun u nad v values, and the corresponding height 
      if (fabs(maxUvel)<fabs(uNew)) {
	maxUvel = fabs(uNew);
	hxCn = hNew;
      }

      if (fabs(maxVvel)<fabs(vNew)) {
	maxVvel = fabs(vNew);
	hyCn = hNew;
      }
    }
  }
}

void ComputeKernel::flood_equation_gpu(DataPacket &h_i, DataPacket& h_o, \
      DataPacket &u_i, DataPacket &u_o, \
      DataPacket &v_i, DataPacket &v_o, \
      DataPacket &dem_values, \
      float dx, float dy, float dt, \
      float hExtra, float manN, float g) {

}

