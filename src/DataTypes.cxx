
#include "DataTypes.h"

template<> int PAIR<int>::minus_one = -1;
template<> float PAIR<float>::minus_one = -1;
template<> double PAIR<double>::minus_one = -1;

MPI_Datatype DataTypes::MPI_BLK_META_DATA = 0;
MPI_Datatype DataTypes::MPI_DEM_HEADER = 0;
MPI_Datatype DataTypes::MPI_VEL_HEIGHT = 0;
MPI_Op DataTypes::MPI_VEL_HEIGHT_MAX = 0;

static void vel_height_max(vel_height *in, vel_height *in_out, int *len,
    MPI_Datatype *datatype) {
  for (int i=0; i<*(len); i++) {
    if ((*in).vel[0]>(*in_out).vel[0]) {
      (*in_out).vel[0] = (*in).vel[0];
      (*in_out).height[0] = (*in).height[0];
    }
    
    if ((*in).vel[1]>(*in_out).vel[1]) {
      (*in_out).vel[1] = (*in).vel[1];
      (*in_out).height[1] = (*in).height[1];
    }

    in++; in_out++;
  }
}

int DataTypes::init() {
  int count = 1;
  MPI_Datatype oldtypes = MPI_INT;
  MPI_Aint offsets = 0;
  int blockcounts = 6;

  if (MPI_Type_struct(count, &blockcounts, &offsets, &oldtypes,
	&MPI_BLK_META_DATA)!=MPI_SUCCESS) {
    printf("MPI_Type_struct failed on MPI_BLK_META_DATA!!!\n");
    return 0;
  }

  if (MPI_Type_commit(&MPI_BLK_META_DATA)!=MPI_SUCCESS) {
    printf("MPI_Type_commit failed on MPI_BLK_META_DATA!!!\n");
    return 0;
  }
  
  count = 2;
  MPI_Datatype _oldtypes[2]; 
  MPI_Aint _offsets[2], extent;
  int _blockcounts[2];

  _oldtypes[0] = MPI_INT;
  _oldtypes[1] = MPI_FLOAT;

  MPI_Type_extent(MPI_INT, &extent);
  _offsets[0] = 0;
  _offsets[1] = 3*extent;

  _blockcounts[0] = 3;
  _blockcounts[1] = 3;

  if (MPI_Type_struct(count, _blockcounts, _offsets, _oldtypes,
	&MPI_DEM_HEADER)!=MPI_SUCCESS) {
    printf("MPI_Type_struct failed on MPI_DEM_HEADER!!!\n");
    return 0;
  }

  if (MPI_Type_commit(&MPI_DEM_HEADER)!=MPI_SUCCESS) {
    printf("MPI_Type_commit failed on MPI_DEM_HEADER!!!\n");
    return 0;
  }

  count = 1;
  oldtypes = MPI_FLOAT;
  blockcounts = 4;
  if (MPI_Type_struct(count, &blockcounts, &offsets, &oldtypes,
	&MPI_VEL_HEIGHT)!=MPI_SUCCESS) {
    printf("MPI_Type_struct failed on MPI_VEL_HEIGHT!!!\n");
    return 0;
  }

  if (MPI_Type_commit(&MPI_VEL_HEIGHT)!=MPI_SUCCESS) {
    printf("MPI_Type_commit failed on MPI_VEL_HEIGHT!!!\n");
    return 0;
  }

  if (MPI_Op_create((MPI_User_function *)vel_height_max, 1,
	&MPI_VEL_HEIGHT_MAX)!=MPI_SUCCESS) {
    printf("MPI_Op_create failed on MPI_VEL_HEIGHT_MAX!!!\n");
    return 0;
  }

  return 1;
}

