
#include "DataDecomposer.h"

#include <float.h>
#include <math.h>
#include <iomanip>
#include <iostream>
using namespace std;

DecomposerNode::DecomposerNode() 
{
}

DecomposerNode::DecomposerNode(INT_PAIR min, INT_PAIR max) :\
  min(min), max(max) 
{
}

DecomposerNode::~DecomposerNode()
{
}

INT_PAIR DecomposerNode::GetMin() const
{
  return min;
}

INT_PAIR DecomposerNode::GetMax() const
{
  return max;
}

DataDecomposer::DataDecomposer(INT_PAIR dim, int axis, int num_procs) : dim(dim), \
  axis(axis), num_procs(num_procs)  
{
  INT_PAIR min(0, 0), max(dim[0], dim[1]);
  Decompose(min, max);
  GenerateProcAllocTable();
}

DataDecomposer::~DataDecomposer() 
{
}

void DataDecomposer::Decompose(INT_PAIR min, INT_PAIR max) {
  int partition_size = ceil((float)(max[axis]-min[axis])/num_procs);

  for (int i=0; i<num_procs; i++) {
    int lower_limit = (i+0)*partition_size;
    int upper_limit = (i+1)*partition_size;

    if (axis==0) {
      if (upper_limit>max[0])
	upper_limit = max[0];
    }
    else if (axis==1) {
      if (upper_limit>max[0])
	upper_limit = max[0];
    }

    INT_PAIR _min, _max;
    if (axis==0) {
      _min = INT_PAIR(lower_limit, max[0]);
      _max = INT_PAIR(upper_limit, max[1]);
    }
    else if (axis==1) {
      _min = INT_PAIR(min[0], lower_limit);
      _max = INT_PAIR(max[0], upper_limit);
    }

    proc_node_map[i] = new DecomposerNode(_min, _max);
  }
}

void DataDecomposer::GenerateProcAllocTable() {
  // Populate the two processor maps
  for (int i=0; i<num_procs; i++) {
    DecomposerNode *node = proc_node_map[i];
    INT_PAIR min = (*node).GetMin();
    INT_PAIR max = (*node).GetMax();

    INT_PAIR range_x(min[0], max[0]);
    INT_PAIR range_y(min[1], max[1]);

    if (proc_x_map.find(range_x)==proc_x_map.end())
      proc_x_map.insert(range_x);
    
    if (proc_y_map.find(range_y)==proc_y_map.end())
      proc_y_map.insert(range_y);
  }

  // Re-size the processor allocation table
  proc_alloc_table.resize(proc_y_map.size());
  for (int i=0 ; i<proc_y_map.size(); i++)
    proc_alloc_table[i].resize(proc_x_map.size());

  // Populate the processor allocation table
  set<INT_PAIR>::iterator range_iterator;
  int range_index_x, range_index_y;  
  for (int i=0; i<num_procs; i++) {
    DecomposerNode *node = proc_node_map[i];
    INT_PAIR min = (*node).GetMin();
    INT_PAIR max = (*node).GetMax();

    INT_PAIR range_x(min[0], max[0]);
    INT_PAIR range_y(min[1], max[1]);

    if ((range_iterator=proc_x_map.find(range_x))!=\
	proc_x_map.end()) {
      range_index_x = distance(proc_x_map.begin(), range_iterator);
    }
    else {
      range_index_x = -1;
    }
    
    if ((range_iterator=proc_y_map.find(range_y))!=\
	proc_y_map.end()) {
      range_index_y = distance(proc_y_map.begin(), range_iterator);
    }
    else {
      range_index_y = -1;
    }

    if (range_index_x!=-1&&range_index_y!=-1)
      proc_alloc_table[range_index_y][range_index_x] = i;
  }
}

const std::vector< std::vector<int> >& DataDecomposer::GetProcAllocTable() const {
  return proc_alloc_table;
}

int DataDecomposer::GetNumBlocksX() const {
  return proc_x_map.size();
}

int DataDecomposer::GetNumBlocksY() const {
  return proc_y_map.size();
}

const DecomposerNode* DataDecomposer::GetProcNode(int proc) {
  if (proc_node_map.find(proc)!=proc_node_map.end())
    return proc_node_map[proc];
  else
    return NULL;
}

