#include "KdTreeNode.h"

KdTreeNode::KdTreeNode() : axes(-1), left(NULL), right(NULL) {
}

KdTreeNode::~KdTreeNode() {
  delete left;
  delete right;
}
