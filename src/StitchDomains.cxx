#include <stdlib.h>
#include <string.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
using namespace std;

static void usage(const char *pgm) {
 fprintf(stderr, "Usage: %s -prefix <h_out>\n \
     -procseq <0 2 1 3>\n \
     -numiter <val>\n \
     -procx <num of procs in x dir>\n \
     -procy <num of procs in y dir>\n \
     -dimx <dimension of a patch in x dir>\n \
     -dimy <dimension of a patch in y dir>\n \
     -ghostx <ghost cells in x dir>\n \
     -ghosty <ghost cells in y dir>\n", pgm);
}

int main(int argc, char **argv) {
  if (argc<6) {
    usage(argv[0]);
    exit(-1);
  }

  const char *file_prefix = NULL;
  string proc_seq;
  bool text_op;
  int num_iter = 0, dim[2], num_procs[2], ghost_cells[2];

  for (int i=1; i<argc; i++) {
    if (strcmp(argv[i], "-prefix")==0) {
      file_prefix = argv[++i];
    }
    else if (strcmp(argv[i], "-procseq")==0) {
      proc_seq = argv[++i];
    }
    else if (strcmp(argv[i], "-numiter")==0) {
      num_iter = atoi(argv[++i]);
    }
    else if (strcmp(argv[i], "-procx")==0) {
      num_procs[0] = atoi(argv[++i]);
    }
    else if (strcmp(argv[i], "-procy")==0) {
      num_procs[1] = atoi(argv[++i]);
    } 
    else if (strcmp(argv[i], "-dimx")==0) {
      dim[0] = atoi(argv[++i]);
    }
    else if (strcmp(argv[i], "-dimy")==0) {
      dim[1] = atoi(argv[++i]);
    }
    else if (strcmp(argv[i], "-ghostx")==0) {
      ghost_cells[0] = atoi(argv[++i]);
    }
    else if (strcmp(argv[i], "-ghosty")==0) {
      ghost_cells[1] = atoi(argv[++i]);
    }
    else if (strcmp(argv[i], "-textop")==0) {
      text_op = true;
    }
    else {
      fprintf(stderr, "Unknown arguement %s\n", argv[i]);
      usage(argv[0]);
      exit(-1);
    }
  }

  // Create a process map
  vector< vector<int> > proc_map;
  proc_map.resize(num_procs[1]);
  for (int i=0; i<num_procs[1]; i++)
    proc_map[i].resize(num_procs[0]);

  // Parse the process sequence
  vector<int> tmp_map; 
  if (!proc_seq.empty()) {
    string tok;
    istringstream iss(proc_seq);

    while (iss>>tok) {
      tmp_map.push_back(atoi(tok.c_str()));
    }
  }

  // Populate the process map
  for (int i=0, cntr=0; i<num_procs[1]; i++) {
    for (int j=0; j<num_procs[0]; j++) {
      proc_map[i][j] = tmp_map[cntr++];
    }
  }
  
  // Print the process map
  printf("Process map\n");
  for (int i=0; i<num_procs[1]; i++) {
    for (int j=0; j<num_procs[0]; j++) {
      cout << proc_map[i][j] << " ";
    }
    cout << endl;
  }

  // The dimension of the stitched dataset
  int s_dim[2], p_dim[2]; 
  s_dim[0] = dim[0]*num_procs[0];
  s_dim[1] = dim[1]*num_procs[1];
  p_dim[0] = dim[0]+2*ghost_cells[0];
  p_dim[1] = dim[1]+2*ghost_cells[1];

  printf("Patch dim_x: %d, dim_y: %d\n", p_dim[0], p_dim[1]); 
  printf("Stitched dim_x: %d, dim_y: %d\n", s_dim[0], s_dim[1]); 

  // ALlocate memory for the stitched dataset
  float *s_ds = new float[s_dim[0]*s_dim[1]];
  float *p_ds = new float[p_dim[0]*p_dim[1]];
  size_t s_siz = s_dim[0]*s_dim[1]*sizeof(float);

  // Read the files from all processes
  char in_file_name[128], op_file_name[128];
  int src_off[2], dst_off[2];
  for (int iter=0; iter<num_iter; iter++) {
    printf("Stitcihng file for iter: %d\n", iter);

    for (int i=0; i<num_procs[1]; i++) {
      for (int j=0; j<num_procs[0]; j++) {
	sprintf(in_file_name, "%s.%d.%04d.log", file_prefix, proc_map[i][j], iter);

	ifstream in_file(in_file_name, ios::in|ios::binary|ios::ate);
	if (in_file.is_open()) {
	  ifstream::pos_type p_siz = in_file.tellg();
	  in_file.seekg(0, ios::beg);
	  in_file.read((char *)p_ds, p_siz);
	  in_file.close();

	  src_off[0] = ghost_cells[0];
	  src_off[1] = ghost_cells[1]; 

	  dst_off[0] = j*dim[0];
	  dst_off[1] = i*dim[1];
	  
	  printf("off[0]: %d, off[1]: %d\n", dst_off[0], dst_off[1]);

	  // Copy one row at a time 
	  for (int m=0; m<dim[1]; m++) {
	    float *src_ptr = p_ds+(src_off[1]+m)*p_dim[0]+src_off[0];
	    float *dst_ptr = s_ds+(dst_off[1]+m)*s_dim[0]+dst_off[0];

	    memcpy(dst_ptr, src_ptr, dim[0]*sizeof(float));
	  }

	  printf("Stitched file %s\n", in_file_name);
	}
	else {
	  printf("Unable to read file %s\n", in_file_name);
	}
      }
    }
    
    sprintf(op_file_name, "stitched_%s.%04d.log", file_prefix, iter);

    if (!text_op) { // Binary output
      ofstream op_file(op_file_name, ofstream::binary); 

      op_file.write((char *)s_ds, s_siz);

      op_file.close(); 
    }
    else { // Text output
      ofstream op_file(op_file_name);

      char tmp_arr[256];
      string line;
      for (int i=0; i<s_dim[1]; i++) {
	for (int j=0; j<s_dim[0]; j++) {
	  float s_val = *(s_ds+(i*s_dim[0])+j);
	  sprintf(tmp_arr, "%0.4f", s_val);
	  line += tmp_arr;

	  if (j<(s_dim[0]-1))
	    line += " ";
	  else
	    line += "\n";
	}

	op_file << line;
	line.clear();
      }

      op_file.close();
    }
      
    printf("Written file %s\n", op_file_name); 
  }

  delete [] s_ds;
  delete [] p_ds;

  return 0;
}
