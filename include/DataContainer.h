
#ifndef _data_container_h
#define _data_container_h

#include <vector>

#include "KdTree.h"
#include "DataDecomposer.h"

class DataContainer {
 private:
   int dim_x, dim_y; // Dimensions of the input dataset
   int num_procs; // Number of processors 
   int num_blks_x, num_blks_y; // Number of blocks in x and y dirextion
   // int blk_size_x, blk_size_y; // Block size of each node, number of procs

   KdTree *kdTreePtr; // KdTree pointer used for partitioning the dataset 
   DataDecomposer *dd_ptr;

   // TODO: All 2D array pointers should (probably) be converted to vectors
   CONTAINER_TYPE **data; // Data container
   std::vector< std::vector<int> > proc_map;
   std::vector< std::vector<INT_PAIR> > offset_map;
   
   void partitionAndGenerateOffsets();
 
 public:
   DataContainer();
   ~DataContainer();
   // DataContainer(int blk_size_x, int blk_size_y, \
   //     int dim_x, int dim_y, int num_procs);

   void instantiate(int dim_x, int dim_y, int num_procs);

   void readDataFromFile(const char *dem_file);

   const std::vector< std::vector<int> >& getProcMap() const;
   const std::vector< std::vector<INT_PAIR>  >& getOffsetMap() const;
   
   int getNumBlocksX() const;
   int getNumBlocksY() const;

   int getBlockSizeX(int i, int j) const;
   int getBlockSizeY(int i, int j) const;

   CONTAINER_TYPE const** getData() const;
};

#endif // End of _data_container_h
