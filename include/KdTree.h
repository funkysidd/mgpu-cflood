
#ifndef _kd_tree_h
#define _kd_tree_h

#include <vector>
#include <map>
#include <set>

#include "KdTreeNode.h"

class KdTree {
 public:
  KdTree(INT_PAIR dim, int num_procs);
  virtual ~KdTree();

  void buildKdTree(KdTreeNode* node, int depth, INT_PAIR min, INT_PAIR max);
  const std::vector< std::vector<int> >& getProcAllocTable() const;  
  int getNumBlocksX() const;  
  int getNumBlocksY() const;
  const KdTreeNode* getProcNode(int proc);   

 private:
  KdTreeNode* root_node;
  INT_PAIR dim;
  int num_procs, tree_depth, proc_count;

  std::map<int, KdTreeNode *> proc_node_map;
  std::vector< std::vector<int> > proc_alloc_table;  
  std::set<INT_PAIR> proc_x_map;
  std::set<INT_PAIR> proc_y_map;
  
  void generateProcAllocTable();
};

#endif
