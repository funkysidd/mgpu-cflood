
#ifndef _kd_tree_node_h
#define _kd_tree_node_h

#include "DataTypes.h"

class KdTreeNode {
 public:
  KdTreeNode();
  ~KdTreeNode();
  
  INT_PAIR min, max;
  KdTreeNode *left, *right;
  int axes, depth;
};

#endif
