
#ifndef _data_decomposer_h
#define _data_decomposer_h

#include <vector>
#include <map>
#include <set>

#include "DataTypes.h"

class DecomposerNode {
 public:
  DecomposerNode();
  DecomposerNode(INT_PAIR min, INT_PAIR max);

  ~DecomposerNode();

  INT_PAIR GetMin() const;
  INT_PAIR GetMax() const;
  
 private:
  INT_PAIR min, max;
};

class DataDecomposer {
 public:
  DataDecomposer(INT_PAIR dim, int axis, int num_procs);
  virtual ~DataDecomposer();

  void Decompose(INT_PAIR min, INT_PAIR max);
  const std::vector< std::vector<int> >& GetProcAllocTable() const;  
  int GetNumBlocksX() const;  
  int GetNumBlocksY() const;
  const DecomposerNode* GetProcNode(int proc);   

 private:
  DecomposerNode* root_node;
  INT_PAIR dim;
  int axis, num_procs;

  // axis: 0 - Partition along the x-axis
  // axis: 1 - Partition along the y-axis

  std::map<int, DecomposerNode *> proc_node_map;
  std::vector< std::vector<int> > proc_alloc_table;  
  std::set<INT_PAIR> proc_x_map;
  std::set<INT_PAIR> proc_y_map;
  
  void GenerateProcAllocTable();
};

#endif
