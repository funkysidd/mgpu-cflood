// textfile.h: interface for reading and writing text files
// www.lighthouse3d.com
//
// You may use these functions freely.
// they are provided as is, and no warranties, either implicit,
// or explicit are given
//////////////////////////////////////////////////////////////////////

extern char *textFileRead(char *fn);
extern int textFileWrite(char *fn, char *s);
extern void WriteOutputFile(float *dataarray, int nRows, int nCols, char *filename); //ADDED BY ALFRED
