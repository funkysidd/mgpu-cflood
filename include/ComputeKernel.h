
#ifndef _compute_kernel_h
#define _compute_kernel_h

#include "DataPacket.h"

class ComputeKernel {
  public:
    static void flood_equation_cpu(DataPacket &h_i, DataPacket& h_o, \
	DataPacket &u_i, DataPacket &u_o, \
	DataPacket &v_i, DataPacket &v_o, \
	DataPacket &dem_values, \
	float dx, float dy, float dt, \
        float hExtra, float manN, float g, \
	int rank, int src_update_proc, \
	int src_row, int col, \
        float &maxUvel, float &maxVvel, \
        float &hxCn, float &hyCn);

    static void flood_equation_gpu(DataPacket &h_i, DataPacket& h_o, \
	DataPacket &u_i, DataPacket &u_o, \
	DataPacket &v_i, DataPacket &v_o, \
	DataPacket &dem_values, \
	float dx, float dy, float dt, \
        float hExtra, float manN, float g);
};

#endif
