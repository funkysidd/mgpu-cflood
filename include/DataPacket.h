
#ifndef _data_packet_h
#define _data_packet_h

#include <vector>

// #include <OpenGLInclude.h>
// #include <GlutInclude.h>
// #include <CudaPbo.h>

#include "DataTypes.h"

#define ACCESS(data, i, j, width) data[i*width+j]

// typedef CudaPbo <FloatArrayTraits> ScalarField;

class DataPacket {
  private:
    /**
      The actual data array has an underlying 1D representaton. The MPI vector
      data type expects data to be in a 1D format; the 'stride' parameter
      doesn't yields the correct element, when each row is a separate
      allocation.
      */

    CONTAINER_TYPE *data;
    // ScalarField data_field;

    int width, height;
    int new_w, new_h; 
    INT_PAIR ghost_cells; // 0 -> (j, x) 1 -> (i, y)

    std::vector< std::vector<int> > proc_map;

    MPI_Datatype column_type, row_type;
    
    bool common_init();
    void ghost_init();
    
    bool interchange_lr(MPI_Comm comm, int rank);
    bool interchange_tb(MPI_Comm comm, int rank);

    bool interchange_co(MPI_Comm comm, int rank);
    bool interchange_d0(MPI_Comm comm, int rank);
    bool interchange_d1(MPI_Comm comm, int rank);

  public:
    DataPacket(int width, int height, INT_PAIR ghost_cells);
    ~DataPacket();

    // Pointers to several locations in in the data container
    CONTAINER_TYPE *_l, *_r, *_t, *_b; // The four edges
    CONTAINER_TYPE *gl, *gr, *gt, *gb; // The ghost edges
    CONTAINER_TYPE *_ll, *_ul, *_lr, *_ur; // The four corners
    CONTAINER_TYPE *gll, *gul, *glr, *gur; // The ghost corners

    // Initialize the pointers and copy the data block
    bool init(const CONTAINER_TYPE *data_blk);
    bool init(const CONTAINER_TYPE val);

    // Test method to increment the value in a DataPacket
    bool incr(const CONTAINER_TYPE val);

    // Set neighborhood map 
    inline void setProcMap(std::vector< std::vector<int> >& _proc_map) {
      proc_map = _proc_map;
    }

    inline int GetPaddedWidth() { return new_w; }
    inline int GetPaddedHeight() { return new_h; }
    inline INT_PAIR GetGhostCells() { return ghost_cells; }

    // Access methods for setting/ retrieving packet data. The 'set'/ 'get'
    // methods are redundant, the 'access' method should be used everywhere.
    CONTAINER_TYPE& access_data(int i, int j);
    CONTAINER_TYPE get_data(int i, int j);
    void set_data(int i, int j, CONTAINER_TYPE val);

   // Interchange data between neighboring processes
   bool interchange(MPI_Comm comm, int rank);

   // Write data to the disk, only the interior cells. The 'iter' parameter is
   // optional and is used to specify a given iteration in a simulation.
   bool WriteToDisk(int rank, const char *prefix, int iter=0);
};

#endif
