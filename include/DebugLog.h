
#ifndef _debug_log_h
#define _debug_log_h

#include <mpi.h>

#define MAX_LOG_BUF_LENGTH 1024

class DebugLog {
 private:
   MPI_File fh;
   MPI_Status status;
   char log_name[256], log_buf[MAX_LOG_BUF_LENGTH];
  
 public:
   DebugLog(MPI_Comm comm, const char *name="debug.log");
   ~DebugLog();

   DebugLog operator<< (const char *data);
   void close();
};

#endif
